require('geckodriver');
require('chromedriver');
const webdriver = require('selenium-webdriver');

const exampleTest = require('./testfunction');

const driver_fx = new webdriver.Builder()
    .forBrowser('firefox')
    .build();

var driver_chr = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

exampleTest.searchTest(driver_fx);
exampleTest.searchTest(driver_chr);

